const std = @import("std");
const Builder = std.build.Builder;

pub fn build(b: *Builder) !void {
    // Standard target options allows the person running `zig build` to choose
    // what target to build for. Here we do not override the defaults, which
    // means any target is allowed, and the default is native. Other options
    // for restricting supported target set are available.
    const target = b.standardTargetOptions(.{});

    // Standard release options allow the person running `zig build` to select
    // between Debug, ReleaseSafe, ReleaseFast, and ReleaseSmall.
    const mode = b.standardReleaseOptions();

    const exe = b.addExecutable("lookupunicode", "src/main.zig");
    exe.setTarget(target);
    exe.setBuildMode(mode);
    const data_path = try std.mem.concat(b.allocator, u8, &[3][]const u8{ "\"", b.option([]const u8, "data_path", "path to the UnicodeData.txt file") orelse "/usr/share/unicode/UnicodeData.txt", "\"" });
    exe.addBuildOption(
        []const u8,
        "data_path",
        data_path,
    );
    b.allocator.free(data_path);
    exe.install();

    const run_cmd = exe.run();
    run_cmd.step.dependOn(b.getInstallStep());
    if (b.args) |args| run_cmd.addArgs(args);

    const run_step = b.step("run", "Run the app");
    run_step.dependOn(&run_cmd.step);
}
