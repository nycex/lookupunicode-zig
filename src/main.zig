const std = @import("std");
const build_options = @import("build_options");

const data = @embedFile(build_options.data_path);

const Char = struct {
    rune: [4]u8,
    rune_length: u3,
    codepoint: []const u8,
    name: []const u8,
};

const Mode = enum {
    Mixed,
    Search,
    Hexadecimal,
    Character,
};

pub fn main() !void {
    const allocator = std.heap.page_allocator;

    const args = try std.process.argsAlloc(allocator);

    std.debug.assert(args.len >= 1);

    if (args.len < 2) {
        try printUsage(args[0], 1);
        unreachable;
    }

    var start_index: u8 = 1;

    const mode: Mode = block: {
        if (std.mem.startsWith(u8, args[1], "-")) {
            start_index += 1;
            if (args.len < 3) {
                const exit_code: u8 = if (std.mem.eql(u8, args[1], "-h") or std.mem.eql(u8, args[1], "--help")) 0 else 1;
                try printUsage(args[0], exit_code);
                unreachable;
            }

            if (std.mem.eql(u8, args[1], "-s") or std.mem.eql(u8, args[1], "--search")) {
                break :block .Search;
            } else if (std.mem.eql(u8, args[1], "-x") or std.mem.eql(u8, args[1], "--hex")) {
                break :block .Hexadecimal;
            } else if (std.mem.eql(u8, args[1], "-c") or std.mem.eql(u8, args[1], "--character")) {
                break :block .Character;
            }
            try printUsage(args[0], 1);
            unreachable;
        } else {
            break :block .Mixed;
        }
    };

    const query = args[start_index..args.len];

    var iterator = std.mem.split(data, "\n");

    var print_by_characters = mode == .Mixed;
    var by_character_chars: std.ArrayList(Char) = undefined;
    if (mode == .Mixed) {
        var query_length: usize = 0;
        for (query) |word| {
            query_length += word.len;
        }
        by_character_chars = try std.ArrayList(Char).initCapacity(allocator, query_length);
    }

    var upper_query: std.ArrayList([]const u8) = undefined;
    if (mode == .Mixed or mode == .Search or mode == .Hexadecimal) {
        upper_query = try std.ArrayList([]const u8).initCapacity(allocator, args.len);
        for (query) |word| {
            const upper = try std.ascii.allocUpperString(allocator, word);
            upper_query.appendAssumeCapacity(upper);
        }
    }

    whileloop: while (iterator.next()) |line| {
        var should_print = false;

        var splitted = std.mem.split(line, ";");
        const codepoint = splitted.next() orelse unreachable;

        if (mode == .Mixed or mode == .Hexadecimal) {
            for (upper_query.items) |word| {
                if (matches(codepoint, word)) {
                    should_print = true;
                    break;
                }
            }
            if (!should_print and mode == .Hexadecimal) {
                continue;
            }
        }

        const name = block: {
            comptime const control_value = "<control>";
            var name_in_file = splitted.next() orelse continue;
            if (std.mem.eql(u8, name_in_file, control_value)) {
                var i: u4 = 0;
                while (i < 8) {
                    _ = splitted.next();
                    i += 1;
                }
                name_in_file = splitted.next() orelse unreachable;
            }
            if (name_in_file.len == 0) {
                if (std.mem.eql(u8, codepoint, "0080")) {
                    break :block "PADDING CHARACTER";
                } else if (std.mem.eql(u8, codepoint, "0081")) {
                    break :block "HIGH OCTET PRESET;";
                } else if (std.mem.eql(u8, codepoint, "0084")) {
                    break :block "INDEX";
                } else if (std.mem.eql(u8, codepoint, "0099")) {
                    break :block "SINGLE GRAPHIC CHARACTER INTRODUCER";
                } else {
                    break :block control_value;
                }
            } else {
                break :block name_in_file;
            }
        };

        if (!should_print and (mode == .Mixed or mode == .Search)) {
            for (upper_query.items) |word| {
                if (!matches(name, word)) {
                    should_print = false;
                    break;
                } else {
                    should_print = true;
                }
            }
            if (!should_print and mode == .Search) {
                continue;
            }
        }

        var rune: [4]u8 = undefined;
        const codepoint_int = try std.fmt.parseInt(u21, codepoint, 16);
        const rune_length = std.unicode.utf8Encode(codepoint_int, &rune) catch |e| switch (e) {
            error.Utf8CannotEncodeSurrogateHalf => block: {
                rune[0] = @intCast(u8, 0b11100000 | (codepoint_int >> 12));
                rune[1] = @intCast(u8, 0b10000000 | ((codepoint_int >> 6) & 0b111111));
                rune[2] = @intCast(u8, 0b10000000 | (codepoint_int & 0b111111));
                break :block 3;
            },
            error.CodepointTooLarge => unreachable,
        };

        if (!should_print) {
            if (print_by_characters and mode == .Mixed) {
                for (query) |word| {
                    var i: usize = 0;
                    while (i < word.len) {
                        const query_rune_length = try std.unicode.utf8ByteSequenceLength(word[i]);
                        if (std.mem.eql(u8, rune[0..rune_length], word[i..][0..query_rune_length])) {
                            by_character_chars.appendAssumeCapacity(Char{
                                .rune = rune,
                                .rune_length = rune_length,
                                .codepoint = codepoint,
                                .name = name,
                            });
                            continue :whileloop;
                        }
                        i += query_rune_length;
                    }
                }
            } else if (mode == .Character) {
                for (query) |word| {
                    var i: usize = 0;
                    while (i < word.len) {
                        const query_rune_length = try std.unicode.utf8ByteSequenceLength(word[i]);
                        if (std.mem.eql(u8, rune[0..rune_length], word[i..][0..query_rune_length])) {
                            should_print = true;
                            break;
                        }
                        i += query_rune_length;
                    }
                }
            }
        }

        if (should_print) {
            print_by_characters = false;
            try printChar(rune, rune_length, codepoint, name);
        }
    }

    if (print_by_characters) {
        for (by_character_chars.items) |char| {
            try printChar(char.rune, char.rune_length, char.codepoint, char.name);
        }
    }
}

fn matches(haystack: []const u8, needle: []const u8) bool {
    const force_start = (std.mem.startsWith(u8, needle, "^"));
    const force_end = (std.mem.endsWith(u8, needle, "$"));

    const start: u1 = if (force_start) 1 else 0;
    const end: usize = if (force_end) needle.len - 1 else needle.len;

    if (force_start) {
        if (force_end) {
            return std.mem.eql(u8, haystack, needle[start..end]);
        } else {
            return std.mem.startsWith(u8, haystack, needle[start..end]);
        }
    } else {
        if (force_end) {
            return std.mem.endsWith(u8, haystack, needle[start..end]);
        } else {
            return std.mem.indexOf(u8, haystack, needle[start..end]) != null;
        }
    }
}

const stdout = std.io.getStdOut().outStream();

fn printChar(rune: [4]u8, runeLength: u3, codepoint: []const u8, name: []const u8) !void {
    try stdout.print("'{}'\t{}\t{}\n", .{ rune[0..runeLength], codepoint, name });
}

fn printUsage(executable: []const u8, exit_code: u8) !void {
    const usage =
        \\Usage: {} [mode] <query>
        \\search in the unicode database
        \\Example: {} -x "^000A$"
        \\
        \\Modes (search by):
        \\  -s, --search    name of the character
        \\  -x, --hex       ascii hexform of the character's codepoint
        \\  -c, --character actual encoded character
        \\
        \\Other:
        \\  -h, --help      show this help page
        \\
    ;
    try stdout.print(usage, .{ executable, executable });
    std.process.exit(exit_code);
}
